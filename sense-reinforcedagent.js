module.exports = function(RED) 
{
    function GetValue(config) 
    {  
        const Agent = require("./nnlib/reinforcedAgent")
        const Network = require("./nnlib/network")

        RED.nodes.createNode(this, config);
        var node = this;

        let layers = JSON.parse(config.layers);

        let network = new Network(layers[0]);

        for(let i = 1; i < layers.length; i++)
        {
            network.addLayer(layers[i], config.activationtype);
        }

        node.context().agent = new Agent(network, config.mutation, config.learnspeed);

        node.on('input', function(msg) 
        {
            switch(msg.topic.toLowerCase())
            {
                case "output":
                    if(typeof msg.payload == "string")
                    {
                        msg.payload = JSON.parse(msg.payload);
                    }

                    msg.payload = node.context().agent.getOutput(msg.payload)
                    node.send(msg);
                    return;
                    break;

                case "fitness":
                    msg.payload = node.context().agent.currentBrain.fitness;
                    node.send(msg);
                    return;
                    break;

                case "tostring":
                    msg.payload = node.context().agent.toString();
                    node.send(msg);
                    return;
                    break;

                case "parse":
                    node.context().agent = Agent.Parse(msg.payload);
                    msg.payload = "Agent parsed and loaded.";
                    node.send(msg);
                    return;
                    break;

                case "good":
                    let evs = node.context().agent.evolutions + node.context().agent.resets;

                    node.context().agent.reward();

                    if (node.context().agent.evolutions + node.context().agent.resets == evs)
                    {
                        msg.payload = "Agent rewarded.";
                    }
                    else
                    {    
                        msg.payload = "Agent rewarded, new iteration started.";
                    }

                    node.send(msg);
                    return;
                    break;

                case "bad":
                    let evsb = node.context().agent.evolutions + node.context().agent.resets;

                    node.context().agent.punish();
                    
                    if (node.context().agent.evolutions + node.context().agent.resets == evsb)
                    {
                        msg.payload = "Agent punished.";
                    }
                    else
                    {    
                        msg.payload = "Agent punished, new iteration started.";
                    }

                    node.send(msg);
                    return;
                    break;

                case "getnetwork":
                    msg.payload = node.context().agent.currentBrain.toString();
                    node.send(msg);
                    return;
                    break;

                case "getbestnetwork":
                    msg.payload = node.context().agent.bestBrain.toString();
                    node.send(msg);
                    return;
                    break;
            }

            msg.payload = "Unrecongnized topic '" + msg.topic + "'";
            node.send(msg);
        });
    }

    RED.nodes.registerType("reinforced-agent", GetValue);
}