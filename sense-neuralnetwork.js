module.exports = function(RED) 
{
    function GetValue(config) 
    {  
        const Network = require("./nnlib/network")

        RED.nodes.createNode(this, config);
        var node = this;

        let layers = JSON.parse(config.layers);

        node.context().network = new Network(layers[0]);

        for(let i = 1; i < layers.length; i++)
        {
            node.context().network.addLayer(layers[i], config.activationtype);
        }

        node.on('input', function(msg) 
        {
            switch(msg.topic)
            {
                case "output":
                    if(typeof msg.payload == "string")
                    {
                        msg.payload = JSON.parse(msg.payload);
                    }

                    msg.payload = node.context().network.getOutputs(msg.payload)
                    node.send(msg);
                    return;
                    break;

                case "mutate":
                    node.context().network.mutate();
                    msg.payload = "Network mutated";
                    node.send(msg);
                    return;
                    break;

                case "fitness":
                    if(typeof msg.payload == "number")
                    {
                        node.context().network.fitness = msg.payload;
                    }

                    msg.payload = node.context().network.fitness;
                    node.send(msg);
                    return;
                    break;

                case "tostring":
                    msg.payload = node.context().network.toString();
                    node.send(msg);
                    return;
                    break;

                case "parse":
                    node.context().network = Network.Parse(msg.payload);
                    msg.payload = "Network parsed and loaded";
                    node.send(msg);
                    return;
                    break;
            }

            msg.payload = "Unrecongnized topic '" + msg.topic + "'";
            node.send(msg);
        });
    }

    RED.nodes.registerType("neural-network", GetValue);
}