"use strict";

class Connection
{
    static totalConnections = 0;

    constructor(neuronFrom, neuronTo, weight = 1)
    {
        this.weight = weight;
        this.neuronFrom = neuronFrom;
        this.neuronTo = neuronTo;

        this.weight = weight;

        this.neuronFrom.connectOut(this);
        this.neuronTo.connectIn(this);

        Connection.totalConnections ++;
    }

    getWeightedNeuronOutput()
    {
        return this.neuronFrom.getValue() * this.weight;
    }

    cleanup()
    {
        Connection.totalConnections--;
    }
}

module.exports = Connection;