"use strict";

const Connection = require("./connection");
const Neuron = require("./neuron");

class Layer
{
    static totalConnections = 0;

    constructor(neuronCount, activationType, input = false)
    {
        this.activationType = activationType;
        this.neurons = [];

        for(let i = 0; i < neuronCount; i++)
        {
            this.addNeuron(new Neuron(activationType, input))
        }
    }

    addNeuron(neuron)
    {
        this.neurons.push(neuron);
    }

    static ConnectLayers(fromLayer, toLayer, randomization = 0)
    {
        for(let i = 0; i < fromLayer.neurons.length; i++)
        {
            if(fromLayer.neurons[i].connectionsOut.length > 0)
            {
                throw "Neuron in FROM layer already connected!";
                return;
            }
        }

        for(let i = 0; i < toLayer.neurons.length; i++)
        {
            if(toLayer.neurons[i].connectionsOut.length > 0)
            {
                throw "Neuron in TO layer already connected!";
                return;
            }
        }

        for(let f = 0; f < fromLayer.neurons.length; f++)
        {
            for(let t = 0; t < toLayer.neurons.length; t++)
            {
                let connection = new Connection(fromLayer.neurons[f], toLayer.neurons[t], 1 + Math.random() * randomization);
            }
        }
    }
}

module.exports = Layer;