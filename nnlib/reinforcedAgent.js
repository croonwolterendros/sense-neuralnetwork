"use strict";

const Network = require("./network")

class ReinforcedAgent
{
    constructor(startBrain, learnSpeed = 1, learnStepSize = 1)
    {
        this.bestBrain = null;
        this.currentBrain = null;
        this.learnCounter = 0;

        this.evolutions = 0;
        this.resets = 0;

        this.improvementsOnly = true; //Set to false to accept any result with Fitness > 0
        this.lowerSuccessBarrierOnFail = true;

        this.bestBrain = startBrain.clone();
        this.currentBrain = startBrain.clone();

        this.bestBrain.mutate(learnSpeed);
        this.currentBrain.mutate(learnSpeed);

        this.bestBrain.fitness = -Number.MAX_VALUE;

        this.learnStepSize = learnStepSize;
        this.learnSpeed = learnSpeed;
    }

    toString()
    {
        let result = "";

        result += this.bestBrain.toString() + "//";          //0 - network
        result += this.currentBrain.toString() + "//";       //1 - network
        result += this.learnStepSize.toString() + "//";      //2 - int
        result += this.learnCounter.toString() + "//";       //3 - int
        result += this.learnSpeed.toString() + "//";         //4 - double
        result += this.evolutions.toString() + "//";         //5 - int
        result += this.resets.toString() + "//";             //6 - int
        result += this.improvementsOnly? "T" : "F" + "//";   //7 - bool
        result += this.lowerSuccessBarrierOnFail? "T" : "F" ; //8 - bool

        return result;
    }

    static Parse(str)
    {
        let result = null;

        try
        {
            let bits = str.split("//");
            let best = Network.Parse(bits[0]);
            let current = Network.Parse(bits[1]);

            result = new ReinforcedAgent(best);

            result.bestBrain = best;
            result.currentBrain = current;
            result.learnStepSize = parseInt(bits[2]);
            result.learnCounter = parseInt(bits[3]);
            result.learnSpeed = parseFloat(bits[4]);
            result.evolutions = parseInt(bits[5]);
            result.resets = parseInt(bits[6]);
            result.improvementsOnly = (bits[7] == "T");
            result.lowerSuccessBarrierOnFail = (bits[8] == "T");
        }
        catch(e)
        {
            throw new FormatException("Agent string was not correctly formatted", e);
        }

        return result;
    }
    
    

    getOutput(inputs)
    {
        return this.currentBrain.getOutputs(inputs);
    }

    reward(reward = 1)
    {
        this.currentBrain.fitness -= -reward;
        this.countLearnStep();
    }

    punish(punishment = 1)
    {
        this.currentBrain.fitness -= punishment;
        this.countLearnStep();
    }

    countLearnStep()
    {
        this.learnCounter++;

        if (this.learnCounter >= this.learnStepSize)
        {
            this.evaluate();
            this.learnCounter = 0;
        }
    }

    evaluate()
    {
        if (this.currentBrain.fitness > this.bestBrain.fitness)
        {
            this.bestBrain = this.currentBrain.clone();
            this.bestBrain.fitness = this.currentBrain.fitness < 0 || this.improvementsOnly ? this.currentBrain.fitness : 0;

            this.evolutions++;
        }
        else
        {
            this.currentBrain = this.bestBrain.clone();
            this.currentBrain.mutate(this.learnSpeed);

            if(this.lowerSuccessBarrierOnFail)
            {
                this.bestBrain.fitness -= Math.abs(this.bestBrain.fitness * 0.05);
            }

            this.resets++;
        }
    }
}

module.exports = ReinforcedAgent;