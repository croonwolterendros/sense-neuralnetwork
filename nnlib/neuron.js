"use strict";

class Neuron
{
    constructor(activationType, input = false)
    {
        this.connectionsIn = [];
        this.connectionsOut = [];

        this.bias = 0;

        this.activationType = activationType;

        this.input = 0;
        this.isInput = input;
    }

    connectIn(connection)
    {
        if(!this.connectionsIn.includes(connection))
        {
            this.connectionsIn.push(connection);
        }
    }

    connectOut(connection)
    {
        if(!this.connectionsOut.includes(connection))
        {
            this.connectionsOut.push(connection);
        }
    }

    disconnect(connection)
    {
        if(this.connectionsIn.includes(connection))
        {
            this.connectionsIn.splice(this.connectionsIn.indexOf(connection), 1);
        }

        if(this.connectionsOut.includes(connection))
        {
            this.connectionsOut.splice(this.connectionsOut.indexOf(connection), 1);
        }
    }

    disconnectAll()
    {
        this.disconnectAllIn();
        this.disconnectAllOut();
    }

    disconnectAllIn()
    {
        for(let i = 0; i < this.connectionsIn.length; i++)
        {
            this.connectionsIn[i].neuronFrom.disconnect(this.connectionsIn[i]);
        }

        this.connectionsIn = [];
    }

    disconnectAllOut()
    {
        for(let i = 0; i < this.connectionsOut.length; i++)
        {
            this.connectionsOut[i].neuronTo.disconnect(this.connectionsOut[i]);
        }

        this.connectionsOut = [];
    }

    getWeightedInputs()
    {
        let val = 0;

        for(let i = 0; i < this.connectionsIn.length; i++)
        {
            val -= -this.connectionsIn[i].getWeightedNeuronOutput();
        }

        return val - -this.bias;
    }

    getValue()
    {
        if(this.isInput)
        {
            return this.input;
        }
        else
        {
            switch (this.activationType) 
            {
                case "linear":
                    return this.getWeightedInputs();                
                    break;
                
                case "relu":
                    return Math.max(0, this.getWeightedInputs());                
                    break;

                case "sigmoid":
                    return 1 / (1 + Math.exp(-this.getWeightedInputs()));                
                    break;

                case "tanh":
                    return Math.tanh(this.getWeightedInputs());                
                    break;

                case "softplus":
                    return Math.log(Math.exp(this.getWeightedInputs()) + 1);                
                    break;
            }
        }
    }
}

module.exports = Neuron;