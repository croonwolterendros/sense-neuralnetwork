"use strict";

const Layer = require("./layer")

class Network
{
    constructor(inputLayerNeuronCount, randomization = 0)
    {
        this.fitness = 0;
        this.layers = [];

        this.layers.push(new Layer(inputLayerNeuronCount, "linear", true))
    }

    addLayer(neuronCount, activationType, randomization = 0)
    {
        let newLayer = new Layer(neuronCount, activationType, false);
        Layer.ConnectLayers(this.layers[this.layers.length - 1], newLayer, randomization);

        this.layers.push(newLayer);

        return newLayer;
    }

    getOutputs(inputs)
    {
        if(this.layers.length < 1)
            throw "No layers in the network!";

        if(inputs.length != this.layers[0].neurons.length)
            throw "Input count doesn't match inputlayer size! (inputs '" + inputs.length + "' != layer.size '" + this.layers[0].neurons.length + "')"
        
        let outputs = [];

        for(let i = 0; i < this.layers[0].neurons.length; i++)
        {
            this.layers[0].neurons[i].input = inputs[i];
        }

        for(let i = 0; i < this.layers[this.layers.length - 1].neurons.length; i++)
        {
            outputs.push(this.layers[this.layers.length - 1].neurons[i].getValue());
        }

        return outputs;
    }

    mutate(strength = 1)
    {
        this.fitness = 0;

        for(let i = 0; i < this.layers.length; i++)
        {
            for(let n = 0; n < this.layers[i].neurons.length; n++)
            {
                this.layers[i].neurons[n].bias += this.randomRange(-strength, strength);

                for(let c = 0; c < this.layers[i].neurons[n].connectionsIn.length; c++)
                {
                    this.layers[i].neurons[n].connectionsIn[c].weight += this.randomRange(-strength, strength);
                }
            }
        }
    }

    randomRange(min, max)
    {
        let d = max - min;
        return min + Math.random() * d;
    }

    clone()
    {
        let newNetwork = new Network(this.layers[0].neurons.length, this.randomization);

        newNetwork.fitness = this.fitness;

        for (let i = 1; i < this.layers.length; i++)
        {
            let layer = newNetwork.addLayer(this.layers[i].neurons.length, this.layers[i].activationType, this.layers[i].randomization);

            for (let n = 0; n < layer.neurons.length; n++)
            {
                layer.neurons[n].bias = this.layers[i].neurons[n].bias;
                layer.neurons[n].activationType = this.layers[i].neurons[n].activationType;

                for (let c = 0; c < layer.neurons[n].connectionsIn.Count; c++)
                {
                    layer.neurons[n].connectionsIn[c].weight = this.layers[i].neurons[n].connectionsIn[c].weight;
                }
            }
        }

        return newNetwork;
    }

    toString()
    {
        let str = this.fitness.toString();

        for(let l = 0; l < this.layers.length; l++)
        {
            let layer = this.layers[l];

            str += ":L:" + layer.activationType + ":l:";

            for(let n = 0; n < layer.neurons.length; n++)
            {
                let neuron = layer.neurons[n];
                str += ":N:" + neuron.bias + ":n:";

                for(let c = 0; c < neuron.connectionsOut.length; c++)
                {
                    str += ":C:" + neuron.connectionsOut[c].weight;
                }
            }
        }

        return str;
    }

    static Parse(networkString)
    {
        try
        {
            let layerStrings = networkString.split(":L:");

            let layerData = [];

            for(let i = 0; i < layerStrings.length - 1; i++)
            {
                layerData.push({activationType: "", neurons: null});

                let layerStr = layerStrings[i + 1];

                let split = layerStr.split(":l:");

                let layerType = split[0];

                layerData[i].activationType = layerType;

                let neuronStrings = split[1].split(":N:");

                layerData[i].neurons = [];

                for(let n = 0; n < neuronStrings.length - 1; n++)
                {
                    layerData[i].neurons.push({bias: 0, connections: []})

                    let neuronStr = neuronStrings[n + 1];

                    let nsplit = neuronStr.split(":n:");

                    let bias = parseFloat(nsplit[0]);

                    layerData[i].neurons[n].bias = bias;

                    let connectionStrings = nsplit[1].split(":C:");

                    for(let c = 0; c < connectionStrings.length - 1; c++)
                    {
                        let connectionStr = connectionStrings[c + 1];

                        let weight = parseFloat(connectionStr);

                        layerData[i].neurons[n].connections.push({weight:0});

                        layerData[i].neurons[n].connections[c].weight = weight;
                    }
                }
            }

            let network = new Network(layerData[0].neurons.length);

            network.Fitness = parseFloat(layerStrings[0]);

            for(let i = 1; i < layerData.length; i++)
            {
                network.addLayer(layerData[i].neurons.length, layerData[i].activationType);
            }

            for(let i = 0; i < network.layers.length; i++)
            {
                let layer = network.layers[i];

                for(let l = 0; l < layer.neurons.length; l++)
                {
                    layer.neurons[l].bias = layerData[i].neurons[l].bias;

                    for(let c = 0; c < layer.neurons[l].connectionsOut.length; c++)
                    {
                        layer.neurons[l].connectionsOut[c].weight = layerData[i].neurons[l].connections[c].weight;
                    }
                }
            }

            return network;
        }
        catch(e)
        {
            throw new FormatException("The provided string is not a valid Network string.", e);
        }
    }
}

module.exports = Network;